#include <netlink/netlink.h>
#include <netlink/socket.h>
#include <netlink/route/link.h>
#include <stdio.h>

int main(void) {
    int err = -1;
    struct nl_sock *netlink_socket = NULL;
    struct nl_cache *link_cache = NULL;
    struct rtnl_link *link = NULL;
    struct nl_addr *addr = NULL;
    char *binary_addr = NULL;
    char *name = NULL;
    int idx = 0;

    /* Allocate netlink socket */
    netlink_socket = nl_socket_alloc();
    if (NULL == netlink_socket) {
        printf("Failed to allocate socket\n");
        goto l_cleanup;
    }

    /* Connect the netlink socket */
    err = nl_connect(netlink_socket, NETLINK_ROUTE);
    if (0 != err) {
        printf("Failed to connect the socket\n");
        goto l_cleanup;
    }

    /* Fetch the desired info from the kernel */
    err = rtnl_link_alloc_cache(netlink_socket, AF_UNSPEC, &link_cache);
    if (0 != err) {
        printf("Failed to fetch interfaces info from kernel\n");
        goto l_cleanup;
    }

    /* Move over all the links, and print their details */
    link = (struct rtnl_link *)nl_cache_get_first(link_cache);
    do {
        /* No more links */
        if (NULL == link) {
            break;
        }

        idx = rtnl_link_get_ifindex(link);
        printf("%d, ", idx);

        name = rtnl_link_get_name(link);
        if (NULL != name) {
            printf("%s, ", name);
        }

        addr = rtnl_link_get_addr(link);
        if (NULL != addr) {
            binary_addr = (char *)nl_addr_get_binary_addr(addr);
            printf("%02x:%02x:%02x:%02x:%02x:%02x",
                   binary_addr[0], binary_addr[1], binary_addr[2],
                   binary_addr[3], binary_addr[4], binary_addr[5]);
        }

        printf("\n");

        link = (struct rtnl_link *)nl_cache_get_next((struct nl_object *)link);
    } while (1);

    /* Indicate success */
    err = 0;

l_cleanup:
    /* Close the opened socket */
    if (NULL != netlink_socket) {
        nl_close(netlink_socket);
    }

    return err;
}
